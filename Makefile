.PHONY: help build run

# self-documented Makefile
# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


IMAGE_NAME := simple_invoice
IMAGE_NAME_DEV := simple_invoice:dev


build: ## Build docker image
	@docker build -t $(IMAGE_NAME) .

build-dev: ## Build docker image for development usage
	@docker build -t $(IMAGE_NAME_DEV) \
		-f Dockerfile.dev .


invoice: ## Start docker container, create invoice and convert it to PDF
	@docker run -it --rm    \
		-v$(shell pwd):/app \
		$(IMAGE_NAME)

CMD ?= bash
run-dev:
	@docker run -it --rm    \
		-v$(shell pwd):/app \
		$(IMAGE_NAME_DEV) $(CMD)

dev: run-dev ## Run docker container for development purposes