# SimpleInvoice

## Purpose

Generate simple PDF invoice

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/)

- copy [.env.example](.env.example) into `.env` file and update
  - commented variables are NOT required, rest are required
    - for details see [settings.py](.settings.py)
  - variables ending with `_LINES` will be sliced into lines delimited with symbol "`|`"

## How to run in Docker and convert to PDF automatically

- Build Docker container

```bash
# Windows PowerShell, Linux & Mac
docker build -t invoices_python .

# Linux
make build
```

- Run Docker container and create invoice report

```bash
# Windows PowerShell, Linux & Mac
docker run -it --rm -v ${PWD}:/app simple_invoice

# Linux
make invoice
```

- Now you can find your report in 'output' directory
