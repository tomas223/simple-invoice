#!/bin/bash

# As this is gonna run in container, output_dir has to be inside of this project
output_dir='output'

# Run python script to create HTML report
python main.py
if [ ! 0 -eq $? ]
then
    >&2 echo "Some error occured while creating HTML report"
    exit 1
fi
printf "HTML report successfully created\n\n"

# Prepare variables
input_html_path=$(ls -t ${output_dir}/*.html | head -1) # finds latest html file
input_html_file_noext=$(basename "${input_html_path}" .html)
output_pdf_path="${output_dir}/${input_html_file_noext}.pdf"

# Convert to PDF
wkhtmltopdf "${input_html_path}" "${output_pdf_path}"

# SUCCESS
printf "\nReport converted to PDF: ${output_pdf_path}\n"
